import {Component, OnInit, HostBinding } from '@angular/core';
import {AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import {Router } from '@angular/router';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'login',
  templateUrl: './login.html',
  styleUrls: ['./login.scss']
})
export class Login {

  public form:FormGroup;
  public email:AbstractControl;
  public password:AbstractControl;
  public submitted:boolean = false;
  
  error: any;
	

  constructor(fb:FormBuilder,public af: AngularFire,private router: Router) {
    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
    });

    this.email = this.form.controls['email'];
    this.password = this.form.controls['password'];
	

	this.af.auth.subscribe(auth => { 
      if(auth) {
        this.router.navigateByUrl('/pages/dashboard');
      }
    });
	
  }

  public onSubmit(formData):void {
    this.submitted = true;
	if(formData.valid) {
      console.log(formData.value);
      this.af.auth.login({
        email: formData.value.email,
        password: formData.value.password
      },
      {
        provider: AuthProviders.Password,
        method: AuthMethods.Password,
      }).then(
        (success) => {
        console.log(success);
        this.router.navigate(['/pages/dashboard']);
      }).catch(
        (err) => {
        console.log(err);
        this.error = err;
      })
    }

  }
}
