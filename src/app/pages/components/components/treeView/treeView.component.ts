import {Component} from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormsModule } from '@angular/forms';
//import { FIREBASE_PROVIDERS, defaultFirebase, firebaseAuthConfig, AuthProviders, AuthMethods } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFire } from 'angularfire2';
import {TreeModel} from 'ng2-tree';

@Component({
  selector: 'tree-view',
  templateUrl: './treeView.html',
})





export class TreeView {

    theItems: FirebaseListObservable<any[]>;
  submitted = false;
  message: any;
  clientName="name";
  clientPhoneNumber="555-555-5555";
  jobType="job";
  estimateDate="2017";
  estimatePrice="0";
  longDescription="now";
  ballpark="0.2";
  timeSubmitted: String = new Date().toISOString();
  // data: {name?: string, text?: string} = any;

 
  
  updatecost(value){
	this.estimatePrice=value;
  }
  updatename(value){
	this.clientName=value;
  }
  updatenum(value){
	this.clientPhoneNumber=value;
  }
  updatejob(value){
	this.jobType=value;
  }
  updateball(value){
	this.ballpark=value;
  }
  updatedate(value){
	this.estimateDate=value;
  }
  


  tree: TreeModel = {
    value: 'Programming languages by programming paradigm',
    children: [
      {
        value: 'Object-oriented programming',
        children: [
          {value: 'Java'},
          {value: 'C++'},
          {value: 'C#'},
        ]
      },
      {
        value: 'Prototype-based programming',
        children: [
          {value: 'JavaScript'},
          {value: 'CoffeeScript'},
          {value: 'Lua'},
        ]
      }
    ]
  };
  
  

  items: FirebaseListObservable<any[]>;
  cname: FirebaseObjectObservable<any[]>;
  
  ctest3: FirebaseObjectObservable<any[]>;
  
  constructor(af: AngularFire) {
    this.ctest3 = af.database.object('/test3');
	
    this.items = af.database.list('/test2');
	
	

	
	this.cname = af.database.object('/template');

	


  }

  
    sendDone(){
	
	this.ctest3.update({Name: this.clientName});
	this.ctest3.update({Phone: this.clientPhoneNumber});
	this.ctest3.update({Job: this.jobType});
	this.ctest3.update({Cost: this.estimatePrice});
	this.ctest3.update({Date: this.estimateDate});
  
  } 

}
