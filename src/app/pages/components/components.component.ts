import {Component} from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'components',
  template: `<router-outlet></router-outlet>`
})
export class Components {
  items: FirebaseListObservable<any[]>;
  constructor(af: AngularFire) {
    this.items = af.database.list('/test2');
  }

}
