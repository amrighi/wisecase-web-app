import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { TreeModule } from 'ng2-tree';

import { AngularFireModule } from 'angularfire2';

import { routing }       from './components.routing';
import { Components } from './components.component';
import { TreeView } from './components/treeView/treeView.component';

export const firebaseConfig = {
  apiKey: "AIzaSyADk8twGuulJw_OMe1Y2gk2Za06Cvbm-Qc",
  authDomain: "wisecase-practice.firebaseapp.com",
  databaseURL: "https://wisecase-practice.firebaseio.com",
  storageBucket: "wisecase-practice.appspot.com",
  messagingSenderId: "148681575405"
};

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    TreeModule,
    routing,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  declarations: [
    Components,
    TreeView,
  ]
})
export class ComponentsModule {}
