import {Component} from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFire } from 'angularfire2';
//import { UserData } from '../../providers/user-data';


@Component({
  selector: 'standard-inputs',
  templateUrl: './standardInputs.html',
})
export class StandardInputs {

  newPostings: FirebaseListObservable<any[]>;

  // newPost: FirebaseListObservable<any[]>;;
  // data: FirebaseListObservable<any[]>;
  theItems: FirebaseListObservable<any[]>;
  submitted = false;
  message: any;
  clientName="name";
  clientPhoneNumber="555-555-5555";
  jobType="job";
  estimateDate="2017";
  estimatePrice="0";
  longDescription="now";
  ballpark="0.1";
  timeSubmitted: String = new Date().toISOString();
  // data: {name?: string, text?: string} = any;

 
  
  updatecost(value){
	this.estimatePrice=value;
  }
  updatename(value){
	this.clientName=value;
  }
  updatenum(value){
	this.clientPhoneNumber=value;
  }
  updatejob(value){
	this.jobType=value;
  }
  updateball(value){
	this.ballpark=value;
  }
  updatedate(value){
	this.estimateDate=value;
  }
  
  

  showBallpark:boolean;
  

  
  changeShowStatus(){
    this.showBallpark = !this.showBallpark;

  }
  


  ctest3: FirebaseObjectObservable<any[]>;
  
  constructor(af: AngularFire) {
    this.ctest3 = af.database.object('/test3');
	this.showBallpark = true;
  }

  sendDone(){
	
	this.ctest3.update({Name: this.clientName});
	this.ctest3.update({Phone: this.clientPhoneNumber});
	this.ctest3.update({Job: this.jobType});
	this.ctest3.update({Cost: this.estimatePrice});
	this.ctest3.update({Date: this.estimateDate});
  
  } 
  
  sendPost() {
      if(this.newPostings) {
          let Postings = {
              // from: this.uid,
              // message: this.message,
              // type: this.message,
              // year: this.year,
              clientName: this.clientName,
              clientPhoneNumber: this.clientPhoneNumber,
              jobType: this.jobType,
              estimateDate: this.estimateDate,
              estimatePrice: this.estimatePrice,
              longDescription: this.longDescription,
              timeSubmitted: this.timeSubmitted
          };
          this.newPostings.push(Postings);
          this.message = "";
      }
  };  
  
}
