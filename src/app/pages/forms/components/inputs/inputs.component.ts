import {Component} from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFire } from 'angularfire2';

@Component({
  selector: 'inputs',
  templateUrl: './inputs.html',
})
export class Inputs {

  ctemplate: FirebaseObjectObservable<any[]>;
  
  constructor(af: AngularFire) {
    this.ctemplate = af.database.object('/template');
	
  }
}
